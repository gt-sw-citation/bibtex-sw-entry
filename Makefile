# Depends: rubber
ORGS = $(wildcard *.org)
TEXS = $(patsubst %.org,%.tex,$(ORGS))
PDFS = $(patsubst %.org,%.pdf,$(ORGS))
MAIN = swentry

all: $(MAIN).pdf biblatex

.PRECIOUS: $(TEXS)

%.tex: %.org
	pandoc -s $< -f org -t latex --toc -o $@

%.pdf: %.tex
ifeq (, $(shell PATH=$(PATH) which rubber))
	echo "No rubber in $(PATH), falling back to basic pdflatex calls"
	pdflatex $<
	biber $*
	pdflatex $<
else
	rubber -m pdftex $<
endif

biblatex: biblio.bib
	$(MAKE) -C biblatex

biblio.bib : $(MAIN).org
	bin/extract-bibtex.pl < $< > $@

clean: $(patsubst %,%/clean,$(TEXS))

%/clean:
	if [ -f $* ]; then rubber -m pdftex --clean $* ; fi
	rm -f $*

distclean: clean
