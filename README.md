# bibtex-sw-entry

## Rationale

Software is a research output that must be cited like articles and data.

This repository contains a proposal for software-specific BibTeX entries in
order to facilitate the citation of software projects, together with a reference implementation:

 - the original white paper is [swentry.org](../swentry.org);
 - the reference BibLaTeX implementation is in the [biblatex](../biblatex/) folder;
 - the documentation of the BibLaTeX implementation is [software-biblatex.pdf](../biblatex/software-biblatex.pdf);
 - the [issues](https://gitlab.inria.fr/gt-sw-citation/bibtex-sw-entry/issues) in this repository track ongoing discussions.

# Key issues

To enable proper citation of software artefacts, using systems like BibTeX, we
need to address the following separate but related issues.

## **Data model**
  The key question here is: *what is the subset of software metadata needed for producing a citation?*
  
  It is a key question, as the data model needs to be able to store all
  the information one may need to prepare a properly formatted citation.
  For this, we need to consider what a citation should look like.
  A list of references to existing examples and approaches can be found in
  [the document citation-styles.org](../citation-styles.org)

  We do not need to represent in a `BibTeX` entry all the possible metadata for
  software: the idea is to keep at hand everything needed for producing a proper
  citation in all contexts of interest, and *rely on a link to an external source*
  for the rest. As an example, the affiliations of the authors are an important
  part of the software metadata, like they are for article metadata, but we do
  not want to show them in a citation, so we do not store them in a BibTeX record.

  The current proposal for the data model consists of the *list of fields* presented
  in [swentry.org](../swentry.org)
	
## **Representation** of the data model
  The key question here is: *what new entries should we have in BibTeX for software?*

  A simple approach would be to complete the `@software` BibTeX entry already
  present in `biblatex` (currently handled like `@misc`) with all the fields
  identified for the data model.

  To avoid creating multiple slightly different copies of a `@software` entry,
  we have instead proposed the four entry types `@software`, `@softwareversion`,
  `@softwaremodule`, `@codefragment` connected with a `crossref` field.  Of
  course, it will be possible to merge all the information into a single entry,
  collapsing the chain, if needed.
  
## **View** of a software citation

  The key question here is: *how should the data from the data model be used in an article citation*?
  
  This is a matter of *bibliographic style*, that can be described
  using the Citation Style Language, or in a BibTeX style (using any of the
  `bibtex`, `biblatex`, `biber` tools around).

  A reference BibLaTeX implementation is now available in the [biblatex](../biblatex/) folder;
